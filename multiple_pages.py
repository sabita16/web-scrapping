# grabbing multiple elements/images across pages.
# # website www.toscrape.com is used which is specifically designed
# # to practice web scrapping for which no need to ask for permission.

# GOAL: get a title of all books (1000 books) which has 2-star ratings

import requests
import bs4
import lxml
# base url is https://books.toscrape.com/catalogue/page-1.html but instead of
# page-numbers curly bracket is used for looping
base_url = 'https://books.toscrape.com/catalogue/page-{}.html'
# requesting the url and making soup
request = requests.get(base_url.format(1))
soup = bs4.BeautifulSoup(request.text, 'lxml')
# (.) denotes the class equal's product_pod
# for showing all the list : print(soup.select(".product_pod"))
products = soup.select(".product_pod")
books = products[0]
print(books)
# looking for class = star-rating Two.
# Note: while using a select class-call there happens to be a space a dot (.)
# should be used to replace a space
print(books.select(".star-rating.Three"))
# the title of the book is selected. There are two options:
# the first title is for image and the second one is for the title
# thus second item or index[1] is chosen and later asking for the title name
print(books.select('a'))
print(books.select('a')[1]['title'])

# Loop for checking in all the books through all the pages
two_star_books = []
for n in range(1, 51):
    all_url = base_url.format(n)
    res = requests.get(all_url)
    soup = bs4.BeautifulSoup(res.text, 'lxml')
    items = soup.select(".product_pod")

    for item in items:
        if len(item.select('.star-rating.Two')) != 0:
            book_title = item.select('a')[1]['title']
            two_star_books.append(book_title)

print(two_star_books)
