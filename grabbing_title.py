# the first thing to do is import the requests
import requests
# the url we choose to get the information from e.g.
result = requests.get("http://www.example.com/")
# we get the response which has the text attribute
print(requests.models.Response)
print(result.text)

# import beautifulsoup and lxml into beautifulsoup which them
# ease us to use the soup object
import bs4
import lxml
# we pass in the result.text with lxml
soup = bs4.BeautifulSoup(result.text, "lxml")
print(soup)
# the main thing we do is select the elements out of it.
# selecting the title
print(soup.select('title')[0].getText())
# selecting the paragraphs and 0index
site_paragraphs = soup.select("p")
print(site_paragraphs[0].getText())