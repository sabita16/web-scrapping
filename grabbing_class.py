# grabbing all the strings in the table of contents in the web page
import bs4
import requests
import lxml

# creating a request and passing the full url
request = requests.get('https://en.wikipedia.org/wiki/Grace_Hopper')
# making a soup out of the text from that request and then using lxml in bs
soup = bs4.BeautifulSoup(request.text, "lxml")
# print(soup) that sows all the contents of the page out of which we cal choose what to select
print(soup.select('.toctext'))
# choosing only the text and not the span in class call
# example selecting only first item
first_item = soup.select('.toctext')[0]
print(first_item.text)

# By using loop
for item in soup.select('.toctext'):
    print(item.text)






