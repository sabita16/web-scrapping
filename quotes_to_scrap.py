# grabbing multiple elements/images across pages.
# website https://quotes.toscrape.com/ is used which is specifically designed
# to practice web scrapping for which no need to ask for permission.
# It consists of various authors with their quotes and related tags.

# First importing all the libraries required to scrape a website
import requests
import bs4
import lxml

# Using request library and beautiful soup to connect with 'https://quotes.toscrape.com/page/{}/'
# and get the text from homepage
req = requests.get('https://quotes.toscrape.com')
# print(req.text)

# Getting all the authors name in the first page.
soup = bs4.BeautifulSoup(req.text, 'lxml')
authors_name = soup.select('.author')
print(authors_name)
# Getting the names of authors only / unique authors
authors = set()
for name in authors_name:
    authors.add(name.text)
print(authors)

# Getting all the quotes in the first page
# quotes = soup.select('.quote') but it shows all the elements.
# only the quotes is needed out of it, Thus, looping is needed with empty list
quotes = []
for quote in soup.select('.quote'):
    quotes.append(quote.text)
print(quotes)

# Inspecting the site and using BeautifulSoup to extract top ten tags
# from the requests text from only the top right of the homepage
tags = soup.select('.tag-item')
# For extracting only the tag/text
for item in tags:
    print(item.text)

# Getting unique authors from all the homepages.
# page_url is https://quotes.toscrape.com/page/1/ but its is only first page. thus, instead of
# page-numbers curly bracket is used for looping
page_url = 'https://quotes.toscrape.com/page/{}/'

all_authors = []
for page in range(1, 11):
    all_url = page_url.format(page)
    res = requests.get(all_url)
    my_soup = bs4.BeautifulSoup(res.text, 'lxml')
    for name in authors_name:
        authors.add(name.text)

print(all_authors)

