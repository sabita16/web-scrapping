# Grabbing image from the website
# First making a request to the page by passing the url, turn it into a soup
import requests
import bs4
import lxml
request = requests.get("https://en.wikipedia.org/wiki/Deep_Blue_(chess_computer)")
soup = bs4.BeautifulSoup(request.text, 'lxml')
# print(soup.select('img')). for all the images in the website
# for only the images within the article
print(soup.select('.thumbimage'))
# selecting only first image
grab = soup.select('.thumbimage')[0]
# for grabbing the link
print(grab['src'])
# downloading the image from the weblink by making a request on the particular link
image_weblink = requests.get('https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Kasparov_Magath_1985_Hamburg-2.png/220px-Kasparov_Magath_1985_Hamburg-2.png')
# print(image_weblink.content) which shows the binary file of the image which cannot be read and write by human.
# python can read and write it and save it in the form of an image
# wp-write binary
f = open('my_image.jpg', 'wb')
f.write(image_weblink.content)
f.close()



